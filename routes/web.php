 <?php

//frontend routes

Route::get('/', 'FrontendController@index')->name('front.index');
Route::get('/about', 'FrontendController@about')->name('front.about');
Route::get('/blog', 'FrontendController@blog')->name('front.blog');
Route::get('/contact', 'FrontendController@contact')->name('front.contact');
Route::get('/shop', 'FrontendController@shop')->name('front.shop');
Route::get('/{id}/tag_base_shop', 'FrontendController@tagBaseShop')->name('front.tag_base_shop');
Route::get('/{id}/shop_single', 'FrontendController@shop_single')->name('front.shop_single');
Route::get('/single', 'FrontendController@single')->name('front.single');


Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/{id}/comment','CommentController@store')->name('front.comment');

    Route::get('/admin', 'BackendController@index');

    //categories route
    Route::prefix('categories')->group(function () {
        Route::get('/', 'CategoriesController@index')->name('categories.index');
        Route::get('/create', 'CategoriesController@create')->name('categories.create');
        Route::get('/{id}/show', 'CategoriesController@show')->name('categories.show');
        Route::post('/store', 'CategoriesController@store')->name('categories.store');
        Route::get('/{id}/edit', 'CategoriesController@edit')->name('categories.edit');
        Route::put('/{id}/update', 'CategoriesController@update')->name('categories.update');
        Route::delete('/{id}', 'CategoriesController@destroy')->name('categories.destroy');
        Route::get('/trash', 'CategoriesController@trash')->name('categories.trash');
        Route::get('/{id}/restore', 'CategoriesController@restore')->name('categories.restore');
        Route::delete('/{id}/delete', 'CategoriesController@delete')->name('categories.delete');
    });
    Route::get('download-categories', 'PdfController@downloadCategories')->name('categories.download');
    Route::get('export-categories-to-excel', 'ExcelController@exportCategories')->name('categories.export');


    //tags route
    Route::prefix('tags')->group(function () {
        Route::get('/', 'TagsController@index')->name('tags.index');
        Route::get('/create', 'TagsController@create')->name('tags.create');
        Route::get('/{id}/show', 'TagsController@show')->name('tags.show');
        Route::post('/store', 'TagsController@store')->name('tags.store');
        Route::get('/{id}/edit', 'TagsController@edit')->name('tags.edit');
        Route::put('/{id}/update', 'TagsController@update')->name('tags.update');
        Route::delete('/{id}', 'TagsController@destroy')->name('tags.destroy');
        Route::get('/trash', 'TagsController@trash')->name('tags.trash');
        Route::get('/{id}/restore', 'TagsController@restore')->name('tags.restore');
        Route::delete('/{id}/delete', 'TagsController@delete')->name('tags.delete');
    });

    //colors route
    Route::prefix('colors')->group(function () {
        Route::get('/', 'ColorController@index')->name('colors.index');
        Route::get('/create', 'ColorController@create')->name('colors.create');
        Route::get('/{id}/show', 'ColorController@show')->name('colors.show');
        Route::post('/store', 'ColorController@store')->name('colors.store');
        Route::get('/{id}/edit', 'ColorController@edit')->name('colors.edit');
        Route::put('/{id}/update', 'ColorController@update')->name('colors.update');
        Route::delete('/{id}', 'ColorController@destroy')->name('colors.destroy');
        Route::get('/trash', 'ColorController@trash')->name('colors.trash');
        Route::get('/{id}/restore', 'ColorController@restore')->name('colors.restore');
        Route::delete('/{id}/delete', 'ColorController@delete')->name('colors.delete');
    });


    //sizes route
    Route::prefix('sizes')->group(function () {
        Route::get('/', 'Sizecontroller@index')->name('sizes.index');
        Route::get('/create', 'Sizecontroller@create')->name('sizes.create');
        Route::get('/{id}/show', 'Sizecontroller@show')->name('sizes.show');
        Route::post('/store', 'Sizecontroller@store')->name('sizes.store');
        Route::get('/{id}/edit', 'Sizecontroller@edit')->name('sizes.edit');
        Route::put('/{id}/update', 'Sizecontroller@update')->name('sizes.update');
        Route::delete('/{id}', 'Sizecontroller@destroy')->name('sizes.destroy');
        Route::get('/trash', 'Sizecontroller@trash')->name('sizes.trash');
        Route::get('/{id}/restore', 'Sizecontroller@restore')->name('sizes.restore');
        Route::delete('/{id}/delete', 'Sizecontroller@delete')->name('sizes.delete');
    });

    //profiles Route
    Route::prefix('profiles')->group(function () {

        Route::get('/', 'ProfileController@index')->name('profiles.index');

        Route::get('/create', 'ProfileController@create')->name('profiles.create');
        Route::get('/{id}/show', 'ProfileController@show')->name('profiles.show');
        Route::post('/store', 'ProfileController@store')->name('profiles.store');
        Route::get('/{id}/edit', 'ProfileController@edit')->name('profiles.edit');
        Route::put('/{id}/update', 'ProfileController@update')->name('profiles.update');
        Route::delete('/{id}', 'ProfileController@destroy')->name('profiles.destroy');
        Route::get('/trash', 'ProfileController@trash')->name('profiles.trash');
        Route::get('/{id}/restore', 'ProfileController@restore')->name('profiles.restore');
        Route::delete('/{id}/delete', 'ProfileController@delete')->name('profiles.delete');
    });


    Route::prefix('products')->group(function () {
        Route::get('/', 'ProductsController@index')->name('products.index');
        Route::get('/create', 'ProductsController@create')->name('products.create');
        Route::post('/store', 'ProductsController@store')->name('products.store');
        Route::get('/{id}/show', 'ProductsController@show')->name('products.show');
        Route::get('/{product}/edit', 'ProductsController@edit')->name('products.edit');
        Route::put('/{product}', 'ProductsController@update')->name('products.update');
        Route::delete('/{id}', 'ProductsController@destroy')->name('products.destroy');
        Route::get('/{id}/restore', 'ProductsController@restore')->name('products.restore');
        Route::delete('/{id}/delete', 'ProductsController@delete')->name('products.delete');
        Route::get('/trash', 'ProductsController@trash')->name('products.trash');
    });


    //Brands Route
    Route::prefix('brands')->group(function () {
        Route::get('/', 'BrandController@index')->name('brands.index');
        Route::get('/create', 'BrandController@create')->name('brands.create');
        Route::get('/{id}/show', 'BrandController@show')->name('brands.show');
        Route::post('/store', 'BrandController@store')->name('brands.store');
        Route::get('/{id}/edit', 'BrandController@edit')->name('brands.edit');
        Route::put('/{brand}', 'BrandController@update')->name('brands.update');
        Route::delete('/{id}', 'BrandController@destroy')->name('brands.destroy');

    });


    //Orders Route
    Route::prefix('orders')->group(function () {
        Route::get('/', 'OrderController@index')->name('orders.index');
        Route::get('/create', 'OrderController@create')->name('orders.create');
        Route::get('/{id}/show', 'OrderController@show')->name('orders.show');
        Route::post('/store', 'OrderController@store')->name('orders.store');
        Route::get('/{id}/edit', 'OrderController@edit')->name('orders.edit');
        Route::put('/{brand}', 'OrderController@update')->name('orders.update');
        Route::delete('/{id}', 'OrderController@destroy')->name('orders.destroy');

    });


});
