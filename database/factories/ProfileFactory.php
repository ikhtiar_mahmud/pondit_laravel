<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Profile::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'middle_name' => $faker->lastName,
        'date_of_birth' => \Carbon\Carbon::now(),
        'gender' => $gender,
        'social_security_number' => $faker->numberBetween($min = 1000, $max = 9000),
        'facility_name' => $faker->name,
        'hispanic_origin' => $faker->name,
        'decedentRace' => true,
        'date_pronounced_dead' => \Carbon\Carbon::now(),
        'time_pronounced_dead' => \Carbon\Carbon::now(),
        'signature_of_person_pronouncing_death' => $faker->name,
        'license_number' => $faker->numberBetween($min = 1000, $max = 9000),
        'date_signed' => \Carbon\Carbon::now(),
        'presumed_death_of_birth' => \Carbon\Carbon::now(),
        'presumed_time_of_death' => \Carbon\Carbon::now(),
        'medical_examiner' => $faker->name,
        'a_immediate_death' => $faker->name,
        'a_due_to' => $faker->name,
        'a_onset_death' => $faker->name,
        'b_sequential_list' => $faker->name,
        'b_due_to' => $faker->name,
        'b_onset_death' => $faker->name,
        'c_underlying_death' => $faker->name,
        'c_due_to' => $faker->name,
        'c_onset_death' => $faker->name,
        'd_last' => $faker->name,
        'd_onset_death' => $faker->name,
        'comment' => $faker->name,
        'performed' => true,
        'autospy_finding' => true,
        'tobacco' => true,
        'ifFemale' => true,
        'manner' => true,
        'date_of_injury' => \Carbon\Carbon::now(),
        'time_of_injury' => \Carbon\Carbon::now(),
        'place_of_injury' => $faker->name,
        'injury_at_work' => true,
        'state' => $faker->name,
        'city_or_town' => $faker->city,
        'street_and_number' => $faker->streetName,
        'apartment' => $faker->name,
        'zip' => $faker->postcode,
        'describe_injury_occurred' => $faker->sentence,
        'specify' => true,
    ];


//    return [
//        'first_name' => $faker->firstName,
//        'last_name' => $faker->lastName,
//        'middle_name' => $faker->lastName,
//        'date_of_birth' => $faker->name,
//        'gender' => $gender,
//        'social_security_number' => $faker->name,
//        'facility_name' => $faker->name,
//        'hispanic_origin' => $faker->name,
//        'date_pronounced_dead' => $faker->name,
//        'time_pronounced_dead' => $faker->name,
//        'signature_of_person_pronouncing_death' => $faker->name,
//        'license_number' => $faker->name,
//        'date_signed' => $faker->name,
//        'presumed_death_of_birth' => $faker->name,
//        'presumed_time_of_death' => $faker->name,
//        'medical_examiner' => $faker->name,
//    ];
});
