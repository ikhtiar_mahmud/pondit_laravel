<?php

namespace App\Http\Controllers;

use App\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        $paginateParPage = 10;
        $pageNumber = request('page');
        if ($pageNumber > 1) {
            $serial = ($pageNumber * $paginateParPage) - $paginateParPage;
        } else {
            $serial = 0;
        }
        $colors = Color::paginate($paginateParPage);
        return view('backend.colors.index', compact('colors', 'serial'));
    }

    public function show($id)
    {
        $color = Color::findOrFail($id);
        return view('backend.colors.show', compact('color'));
    }

    public function create()
    {
        return view('backend.colors.create');
    }

    public function store(Request $request)
    {
        Color::create($request->all());
        return redirect()->route('colors.index')->withMessage('color Added Successfully');
    }

    public function edit($id)
    {
        $color = Color::findOrFail($id);
        return view('backend.colors.edit', compact('color'));

    }

    public function update(Request $request, $id)
    {
        $color = Color::findOrFail($id);
        $color->update($request->all());
        return redirect()->route('colors.index')->withMessage('color Update Successfully');
    }

    public function destroy($id)
    {
        $color = Color::findOrFail($id);
        $color->delete();
        return redirect()->route('colors.index')->withMessage('color Deleted Successfully');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $colors = Color::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.colors.trash', compact('colors', 'serial'));
    }

    public function restore($id)
    {
        Color::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
        return redirect()
            ->route('colors.trash')
            ->withMessage('Color Restore Successfully');
    }

    public function delete($id)
    {
        $colors = Color::onlyTrashed()->where('id', $id)->first();
        $colors->forceDelete();
        return redirect()->route('colors.trash')->withMessage('Color Delete Successfully');
    }
}
