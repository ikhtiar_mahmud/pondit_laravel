<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;

use App\Order;
use App\Product;
use App\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;



class OrderController extends Controller
{

    public function index()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $orders = Order::paginate($paginatePerPage);

        return view('backend.orders.index', compact('orders','serial'));
    }


    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $data['created_by'] = auth()->user()->id;
            $data['modified_by'] = auth()->user()->id;

            $product = Product::create($data);
            $product->tags()->attach($request->tag_id);
            return redirect()->route('products.index')->withMessage('Product Inserted Successfully.');
        } catch (QueryException $e) {
            return redirect()
                ->route('categories.create')
                ->withInput()
                ->withErrors($e->getMessage());

        }
    }

    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('backend.orders.show', compact('order'));
    }



    public function edit(Order $order)
    {
        $orders = Order::pluck('name', 'id');

        return view('backend.orders.edit', compact('orders'));
    }


    public function destroy($id)
    {
        $product = Order::findOrFail($id);
        $product->delete();
        return redirect()->route('orders.index')->withMessage('Orders Deleted Successfully.');
    }
}
