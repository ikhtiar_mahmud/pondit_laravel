<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Http\Requests\ProductRequest;
use App\Product;
use App\Size;
use App\Tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;


class ProductsController extends Controller
{
    const UPLOAD_DIR = '/uploads/products/';

    public function index()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $products = Product::paginate($paginatePerPage);

        return view('backend.products.index', compact('products', 'serial'));
    }

    public function create()
    {
        $categories = Category::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id')->toArray();
        $brand = Brand::pluck('name', 'id');
        $sizes = Size::pluck('title', 'id');
        $selectedTagIds = [];
        $selectedSizeIds = [];

        return view('backend.products.create', compact('categories', 'tags', 'selectedTagIds', 'brand', 'sizes', 'selectedSizeIds'));
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('image')) {
                $data['image'] = $this->uploadImage($request->image, $request->title);
            }

            $data['created_by'] = auth()->user()->id;
            $data['modified_by'] = auth()->user()->id;

            $product = Product::create($data);
            $product->tags()->attach($request->tag_id);
            $product->sizes()->attach($request->size_id);
            return redirect()->route('products.index')->withMessage('Product Inserted Successfully.');
        } catch (QueryException $e) {
            return redirect()
                ->route('products.create')
                ->withInput()
                ->withErrors($e->getMessage());

        }
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('backend.products.show', compact('product'));
    }

    public function edit(Product $product)
    {
        $brand = Brand::pluck('name', 'id');
        $categories = Category::pluck('title', 'id');
        $tags = Tag::pluck('title', 'id')->toArray();
        $sizes = Size::pluck('title', 'id')->toArray();


        $selectedTagIds = $product->tags->pluck('id')->toArray();
        $selectedSizeIds = $product->sizes->pluck('id')->toArray();

        return view('backend.products.edit', compact('product', 'categories', 'tags', 'selectedTagIds', 'brand', 'selectedSizeIds', 'sizes'));
    }

    public function update(ProductRequest $request, Product $product)
    {
        $data = $request->all();
        if ($request->hasFile('image')) {
            $data['image'] = $this->uploadImage($request->image, $request->title);
        } else {
            $data['image'] = $product->image;
        }
        $data['modified_by'] = auth()->user()->id;
        $product->update($data);
        $product->tags()->sync($request->tag_id);
        $product->sizes()->sync($request->size_id);
        return redirect()->route('products.index')->withMessage('Product Update Successfully.');
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return redirect()->route('products.index')->withMessage('Product Deleted Successfully.');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $products = Product::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.products.trash', compact('products', 'serial'));
    }

    public function restore($id)
    {
        Product::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
//        $products->restore();
        return redirect()
            ->route('products.trash')
            ->withMessage('Product Restore Successfully');

    }

    public function delete($id)
    {
        $product = Product::onlyTrashed()->where('id', $id)->first();
        $image_path = public_path() . '/uploads/products/' . $product->image;
        unlink($image_path);
        $product->forceDelete();
        $product->tags()->detach();
        $product->sizes()->detach();
        return redirect()->route('products.trash')->withMessage('Product Delete Successfully');
    }


    private function uploadImage($file, $title)
    {
        $extension = $file->getClientOriginalExtension($file);
        $fileName = date('y-m-d') . '_' . time() . '_' . $title . '.' . $extension;
//        $file->move(public_path() . self::UPLOAD_DIR, $fileName);
        Image::make($file)->resize(300, 300)->save(public_path() . self::UPLOAD_DIR . $fileName);
        return $fileName;
    }

}
