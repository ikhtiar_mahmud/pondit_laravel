<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Product;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        $comments = Product::findOrFail($id);
        $comment = new Comment();
        $comment->user_id = auth()->user()->id;
        $comment->body = $request->comment;
        $comments->comments()->save($comment);
        return redirect()->back()->withMessage('Comment Successful');
    }

//    public function index(){
//
//        return view('frontend.shop_single',compact('comments'));
//    }
}
