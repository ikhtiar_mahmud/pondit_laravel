<?php

namespace App\Http\Controllers;

use App\Size;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index()
    {
        $paginateParPage = 10;
        $pageNumber = request('page');
        if ($pageNumber > 1) {
            $serial = ($pageNumber * $paginateParPage) - $paginateParPage;
        } else {
            $serial = 0;
        }
        $sizes = Size::paginate($paginateParPage);
        return view('backend.sizes.index', compact('sizes', 'serial'));
    }

    public function show($id)
    {
        $size = Size::findOrFail($id);
        return view('backend.sizes.show', compact('size'));
    }

    public function create()
    {
        return view('backend.sizes.create');
    }

    public function store(Request $request)
    {
        Size::create($request->all());
        return redirect()->route('sizes.index')->withMessage('size Added Successfully');
    }

    public function edit($id)
    {
        $size = Size::findOrFail($id);
        return view('backend.sizes.edit', compact('size'));

    }

    public function update(Request $request, $id)
    {
        $size = Size::findOrFail($id);
        $size->update($request->all());
        return redirect()->route('sizes.index')->withMessage('size Update Successfully');
    }

    public function destroy($id)
    {
        $size = Size::findOrFail($id);
        $size->delete();
        return redirect()->route('sizes.index')->withMessage('size Deleted Successfully');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $sizes = Size::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.sizes.trash', compact('sizes', 'serial'));
    }

    public function restore($id)
    {
        Size::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
        return redirect()
            ->route('sizes.trash')
            ->withMessage('size Restore Successfully');
    }

    public function delete($id)
    {
        $sizes = Size::onlyTrashed()->where('id', $id)->first();
        $sizes->forceDelete();
        return redirect()->route('sizes.trash')->withMessage('Size Delete Successfully');
    }
}
