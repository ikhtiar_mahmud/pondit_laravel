<?php

namespace App\Http\Controllers;

use App\Brand;
use Illuminate\Http\Request;

use Image;

class BrandController extends Controller
{
    const UPLOAD_DIR = '/uploads/brands/';

    public function index()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $brands = Brand::all();
        return view('backend.brands.index', compact('brands', 'serial'));
    }

    public function show($id)
    {
        $brands = Brand::FindOrFail($id);
        return view('backend.brands.show', compact('brands'));
    }

    public function create()
    {
        return view('backend.brands.create');
    }

    public function store(Request $request)
    {
        try {
            $data = $request->all();
            if ($request->hasFile('logo')) {
                $data['logo'] = $this->uploadImage($request->logo, $request->title);
            }
            $data['created_by'] = auth()->user()->id;
            $data['updated_by'] = auth()->user()->id;
            Brand::create($data);
            return redirect()->route('brands.index')->withMessage('Brand Inserted Successfully.');
        } catch (QueryException $e) {
            return redirect()
                ->route('brands.create')
                ->withInput()
                ->withErrors($e->getMessage());

        }
    }


//    public function edit(Brand $brands)
    public function edit($id)
    {
        $brands = Brand::findOrFail($id);
        return view('backend.brands.edit', compact('brands'));
    }

    public function update(Request $request, Brand $brand)
    {
        $data = $request->all();
        if ($request->hasFile('logo')) {
            $data['logo'] = $this->uploadImage($request->logo, $request->title);
        } else {
            $data['logo'] = $brand->logo;
        }
        $brand->update($data);
        return redirect()->route('brands.index')->withMessage('Brand Update Successfully.');
    }


    public function destroy($id)
    {
        $brands = Brand::findOrFail($id);
        $brands->delete();
        return redirect()->route('brands.index')->withMessage('Brand Deleted Successfully.');
    }


    private function uploadImage($file, $title)
    {
        $extension = $file->getClientOriginalExtension($file);
        $fileName = date('y-m-d') . '_' . time() . '_' . $title . '.' . $extension;
        $file->move(public_path() . self::UPLOAD_DIR, $fileName);
//        Image::make($file)->resize(100,100)->save(public_path().self::UPLOAD_DIR.$fileName);
        return $fileName;
    }

}
