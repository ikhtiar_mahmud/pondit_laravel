<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Tag;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $paginateParPage = 10;
        $pageNumber = request('page');
        if ($pageNumber > 1) {
            $serial = ($pageNumber * $paginateParPage) - $paginateParPage;
        } else {
            $serial = 0;
        }
        $tags = Tag::paginate($paginateParPage);
        return view('backend.tags.index', compact('tags', 'serial'));
    }

    public function show($id)
    {
        $tag = Tag::findOrFail($id);
        return view('backend.tags.show', compact('tag'));
    }

    public function create()
    {
        return view('backend.tags.create');
    }

    public function store(Request $request)
    {
//        dd($request);
        Tag::create($request->all());
        return redirect()->route('tags.index')->withMessage('tag Added Successfully');
    }

    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('backend.tags.edit', compact('tag'));

    }

    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $tag->update($request->all());
        return redirect()->route('tags.index')->withMessage('tag Update Successfully');
    }

    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();
        return redirect()->route('tags.index')->withMessage('tag Deleted Successfully');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $tags = Tag::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.tags.trash', compact('tags', 'serial'));
    }

    public function restore($id)
    {
        Tag::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
        return redirect()
            ->route('tags.trash')
            ->withMessage('tag Restore Successfully');
    }

    public function delete($id)
    {
        $tags = Tag::onlyTrashed()->where('id', $id)->first();
        $tags->forceDelete();
        return redirect()->route('tags.trash')->withMessage('tags Delete Successfully');
    }

}
