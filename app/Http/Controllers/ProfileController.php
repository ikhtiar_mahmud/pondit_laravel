<?php

namespace App\Http\Controllers;

use App\Product;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ProfileRequest;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkCountry')->only('create');
    }

    public function index()
    {
        $paginateParPage = 10;
        $pageNumber = request('page');
        if ($pageNumber > 1) {
            $serial = ($pageNumber * $paginateParPage) - $paginateParPage;
        } else {
            $serial = 0;
        }
        $profiles = Profile::paginate($paginateParPage);
        return view('backend.profiles.index', compact('profiles', 'serial'));
    }

    public function show(Profile $profile)
    {
        //$profile = Profile::findOrFail($id);
        return view('backend.profiles.show', compact('profile'));
    }

    public function create()
    {
        return view('backend.profiles.create');
    }

    public function store(ProfileRequest $request)
    {

        Profile::create($request->all());
        return redirect()->route('profiles.index')->withMessage('Item Inserted successfully');
    }

    public function edit(Profile $profile)
    {
        //$profile = Profile::FindOrFail($id);
        return view('backend.profiles.edit', compact('profile'));
    }

    public function update(Request $request, Profile $profile)
    {
        //$profile = Profile::findOrFail($id);
        $profile->update($request->all());
        return redirect()->route('profiles.index')->withMessage('Item Updated Successfully');
    }

    public function destroy(Profile $profile){
        //$profile=Profile::findOrFail($id);
        $profile->delete();
        return redirect()->route('profiles.index')->withMessage('Item Deleted Successfully');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $profiles = Profile::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.profiles.trash', compact('profiles', 'serial'));
    }

    public function restore($id)
    {
        Profile::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
        return redirect()->route('profiles.trash')->withMessage('Profile Restore Successfully');

    }
    public function delete($id)
    {
        $profile = Profile::onlyTrashed()->where('id', $id)->first();
        $profile->forceDelete();
        return redirect()->route('profiles.trash')->withMessage('Profile Delete Successfully');
    }



}
