<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;

class CategoriesController extends Controller
{
    public function __construct()
    {
//        $this->middleware('checkAge')->except('index');
//        $this->middleware('checkAge');
    }


    public function index()
    {
        $paginateParPage = 10;
        $pageNumber = request('page');
        if ($pageNumber > 1) {
            $serial = ($pageNumber * $paginateParPage) - $paginateParPage;
        } else {
            $serial = 0;
        }
        $categories = Category::paginate($paginateParPage);
        return view('backend.categories.index', compact('categories', 'serial'));
    }

    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.categories.show', compact('category'));
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        Category::create($request->all());
        return redirect()->route('categories.index')->withMessage('Category Added Successfully');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.categories.edit', compact('category'));

    }

    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->update($request->all());
        return redirect()->route('categories.index')->withMessage('Category Update Successfully');
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect()->route('categories.index')->withMessage('Category Deleted Successfully');
    }

    public function trash()
    {
        $paginatePerPage = 10;
        $pagenumber = request('page');
        if ($pagenumber > 1) {
            $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
        } else {
            $serial = 0;
        }
        $categories = Category::onlyTrashed()->orderBy('created_at', 'desc')->paginate($paginatePerPage);
        return view('backend.categories.trash', compact('categories', 'serial'));
    }

    public function restore($id)
    {
        Category::onlyTrashed()
            ->where('id', $id)
            ->first()
            ->restore();
        return redirect()
            ->route('categories.trash')
            ->withMessage('Category Restore Successfully');
    }

    public function delete($id)
    {
        $categories = Category::onlyTrashed()->where('id', $id)->first();
        $categories->forceDelete();
        return redirect()->route('categories.trash')->withMessage('categories Delete Successfully');
    }


}
