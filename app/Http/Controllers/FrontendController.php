<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Comment;
use App\Product;
use App\Tag;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        $products = Product::latest()->take(9)->get();
        return view('frontend.index', compact('products'));
    }

    public function shop_single($id)
    {
        $product = Product::findOrFail($id);
        $comments=Comment::all();
        $categories=Category::all();
        $brands=Brand::all();
        $discounts=Product::all();
        return view('frontend.shop_single', compact('product','categories','comments','brands','discounts'));
    }

    public function about()
    {
        return view('frontend.about');
    }

    public function blog()
    {
        return view('frontend.blog');
    }

    public function contact()
    {
        return view('frontend.contact');
    }

    public function tagBaseShop($id)
    {
        $categories=Category::all();
        $products=Tag::findOrFail($id);
        $brands=Brand::all();
        $discounts=Product::all();
        return view('frontend.tag_base_shop',compact('categories','products','brands','discounts'));
    }

    public function shop()
    {
        $products= Product::all();
        return view('frontend.shop',compact('products'));
    }


    public function single()
    {
        return view('frontend.single');
    }
}
