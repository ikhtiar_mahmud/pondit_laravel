<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
//use Excel;

class ExcelController extends Controller
{
    public function exportCategories()
    {
        $categories = Category::all('id','title')->toArray();
//        dd($categories);
        return Excel::create('categories.',function($excel) use ($categories){
            $excel->sheet('mySheet',function($sheet) use ($categories){
                $sheet->fromArray($categories);
            });
        })->download("xlsx");
    }

}
