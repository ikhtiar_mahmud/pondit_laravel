<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function downloadCategories()
    {
        $categories = Category::all();
        $pdf = PDF::loadView('downloads.category-list', compact('categories'));
        return $pdf->download('category-list.pdf');
//        return view('downloads.category-list', compact('categories'));
    }
}
