<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    protected $fillable = ['facebook_link', 'user_id', 'twitter_link', 'date_of_birth', 'gender', 'social_security_number', 'facility_name', 'hispanic_origin', 'decedentRace', 'date_pronounced_dead', 'time_pronounced_dead', 'signature_of_person_pronouncing_death', 'license_number', 'date_signed', '', 'presumed_death_of_birth', 'presumed_time_of_death', 'medical_examiner', 'ifFemale', 'manner', 'date_of_injury', 'time_of_injury', 'place_of_injury', 'injury_at_work', 'state', 'city_or_town', 'street_and_number', 'apartment', 'zip', 'describe_injury_occurred', 'specify'];

//    use softDeletes;
//    protected $guarded = [];

     //inverse relationship
      public function user()
      {
          return $this->belongsTo(User::class);
      }
}
