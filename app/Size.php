<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Size extends Model
{
    use softDeletes;
    protected $guarded = [];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
