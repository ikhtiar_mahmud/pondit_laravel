<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tag extends Model
{
//    use softDeletes;
    protected  $fillable = ['title', 'created_by', 'updated_by'];

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
