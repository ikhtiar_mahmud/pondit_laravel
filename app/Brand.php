<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    // protected $fillable = ['short_name','name','description','logo'];
    protected $guarded = [];


    public function products()
    {
        return $this->hasMany(Product::class);
    }

    // polymorphic 1 to n
    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');

    }
}
