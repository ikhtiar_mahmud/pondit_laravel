@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>colors Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @if(session('message'))
                    <div class="alert alert-success">
                        {{{session('message')}}}
                    </div>
                @endif
                <div class="text-left" style="margin-bottom: 20px">
                    <a href="{{ route('colors.trash')}}" class="btn btn-success">Go to Trash</a>
                </div>
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('colors.create')}}" class="btn btn-success">Add New colors</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($colors as $color)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$color->title}}</td>
                            <td><a href="{{ route('colors.show',$color->id) }}" class="btn btn-primary">Show</a>
                                <a
                                        href="{{ route('colors.edit',$color->id) }}" class="btn btn-warning">Edit</a>
                                <form action="{{route('colors.destroy',$color->id)}}" method="post"
                                      style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$colors->links()}}

        </div>
    </div>
@endsection
