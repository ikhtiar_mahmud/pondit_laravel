@extends('backend.layouts.master')
@section('content')

    <div class="agile-grids">
        <!-- input-forms -->
        <div class="grids">
            <div class="progressbar-heading grids-heading">
                <h2>Input Forms</h2>
            </div>
            <div class="panel panel-widget forms-panel">
                <div class="forms">
                    <div class="form-grids widget-shadow" data-example-id="basic-forms">
                        <div class="form-title">
                            <h4>Basic Form :</h4>
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="form-body">
                            {{ Form::model($category,['route'=>['categories.update',$category->id],'method'=>'put','enctype'=>'multipart/form-data'])}}

                            @include('backend.categories.form')

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //input-forms -->
    </div>

@endsection