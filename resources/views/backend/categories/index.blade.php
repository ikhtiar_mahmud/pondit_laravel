@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Basic Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @if(session('message'))
                    <div class="alert alert-success">
                        {{{session('message')}}}
                    </div>
                @endif
                <div class="text-left" style="margin-bottom: 20px">
                    <a href="{{ route('categories.trash')}}" class="btn btn-success">Go to Trash</a>
                </div>
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('categories.export')}}" class="btn btn-warning float-left">Excel Download</a>
                    <a href="{{route('categories.download')}}" class="btn btn-danger float-left">PDF Download</a>
                    <a href="{{route('categories.create')}}" class="btn btn-success">Add New Categories</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$category->title}}</td>
                            <td><a href="{{ route('categories.show',$category->id) }}" class="btn btn-primary">Show</a>
                                <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-warning">Edit</a>
                                <form action="{{route('categories.destroy',$category->id)}}" method="post"
                                      style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$categories->links()}}

        </div>
    </div>
@endsection
