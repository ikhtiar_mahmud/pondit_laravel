@extends('backend.layouts.master')
@section('content')


    <div class="card bg-white">
        <div class="card-header bg-info">
            <a href="{{ url('/categories') }}" class="btn btn-success">List</a>
        </div>
        <div class="card-body">
            <p><strong>Title : </strong> {{ $category->title }}</p>

        </div>
        <div class="card-footer"></div>
    </div>


@endsection