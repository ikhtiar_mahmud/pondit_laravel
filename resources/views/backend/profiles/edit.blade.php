@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- input-forms -->
        <div class="grids">
            <div class="progressbar-heading grids-heading">
                <h2>Profiles Forms</h2>
            </div>
            <div class="panel panel-widget forms-panel">
                <div class="forms">
                    <div class="form-grids widget-shadow" data-example-id="basic-forms">
                        <div class="form-title">
                            <h4>Basic Form :</h4>
                        </div>
                        <div class="form-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{route('profiles.update',$profile->id)}}" method="post">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label for="fname">First Name</label>
                                    <input type="text" name="first_name" class="form-control" id="fname"
                                           value="{{$profile->first_name}}">
                                </div>

                                <div class="form-group">
                                    <label for="lname">Last Name</label>
                                    <input type="text" name="last_name" class="form-control" id="lname"
                                           value="{{$profile->last_name}}">
                                </div>

                                <div class="form-group">
                                    <label for="mname">Middle Name</label>
                                    <input type="text" name="middle_name" class="form-control" id="mname"
                                           value="{{$profile->middle_name}}">
                                </div>

                                <div class="form-group">
                                    <label for="Date_of_birth">Date of Birth</label>
                                    <input type="date" name="Date_of_birth" class="form-control" id="Date_of_birth"
                                           value="{{$profile->date_of_birth}}">
                                </div>

                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <select id="gender" name="gender" class="form-control">
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                        <option value="others">Others</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="social_security_number">Social Security Number</label>
                                    <input type="text" name="social_security_number" class="form-control"
                                           id="social_security_number"
                                           value="{{$profile->social_security_number}}">
                                </div>

                                <div class="form-group">
                                    <label for="facility_name">Facility Name</label>
                                    <input type="text" name="facility_name" class="form-control" id="facility_name"
                                           value="{{$profile->facility_name}}">
                                </div>

                                <div class="form-group">
                                    <label for="hispanic_origin">Hispanic Origin</label>
                                    <input type="radio" name="hispanic_origin" value="NO,not Spanish/Hispanic/Latino">NO,not
                                    Spanish/Hispanic/Latino
                                    <input type="radio" name="hispanic_origin"
                                           value="Yes,Mexican,Mexican American,Chicano">Yes,Mexican,Mexican
                                    American,Chicano
                                    <input type="radio" name="hispanic_origin" value="Yes,Puerto Rican">Yes,Puerto Rican
                                    <input type="radio" name="hispanic_origin" value="Yes,Cuban">Yes,Cuban
                                    <input type="radio" name="hispanic_origin" value="Yes,Other Spanish">Yes,Other
                                    Spanish
                                </div>

                                <div class="form-group">
                                    <label>Decedent's Race</label><br>
                                    <input type="radio" name="decedentRace" value="White">White
                                    <input type="radio" name="decedentRace" value="Filipino">Filipino
                                    <input type="radio" name="decedentRace" value="Native Hawaiian">Native Hawaiian
                                    <input type="radio" name="decedentRace" value="Black or African American">Black or
                                    African American
                                    <input type="radio" name="decedentRace" value="Japanese">Japanese
                                    <input type="radio" name="decedentRace" value="Guamanian or Chamorro">Guamanian or
                                    Chamorro
                                    <input type="radio" name="decedentRace" value="American Indian or Alaska Native">American
                                    Indian or Alaska Native
                                    <input type="radio" name="decedentRace" value="Korean">Korean
                                    <input type="radio" name="decedentRace" value="Samoan">Samoan
                                    <input type="radio" name="decedentRace" value="Vietnamese">Vietnamese
                                    <input type="radio" name="decedentRace" value=">Other Pacific Islander">Other
                                    Pacific Islander
                                    <input type="radio" name="decedentRace" value="Asian Indian">Asian Indian
                                    <input type="radio" name="decedentRace" value="Other Asian">Other Asian
                                    <input type="radio" name="decedentRace" value="Other">Other
                                    <input type="radio" name="decedentRace" value="Chinese">Chinese
                                </div>

                                <div class="form-group">
                                    <label for="date_pronounced_dead">Date Pronounced Dead</label>
                                    <input type="date" name="date_pronounced_dead" class="form-control"
                                           id="date_pronounced_dead"
                                           value="{{$profile->date_pronounced_dead}}">
                                </div>

                                <div class="form-group">
                                    <label for="time_pronounced_dead">Time Pronounced Dead</label>
                                    <input type="time" name="time_pronounced_dead" class="form-control"
                                           id="time_pronounced_dead"
                                           value="{{$profile->time_pronounced_dead}}">
                                </div>

                                <div class="form-group">
                                    <label for="signature_of_person_pronouncing_death">Signature of Pronouncing
                                        Death</label>
                                    <input type="text" name="signature_of_person_pronouncing_death" class="form-control"
                                           id="signature_of_person_pronouncing_death"
                                           value="{{$profile->signature_of_person_pronouncing_death}}">
                                </div>

                                <div class="form-group">
                                    <label for="license_number">license_number</label>
                                    <input type="text" name="license_number" class="form-control" id="license_number"
                                           value="{{$profile->license_number}}">
                                </div>

                                <div class="form-group">
                                    <label for="date_signed">Date Signed</label>
                                    <input type="date" name="date_signed" class="form-control" id="date_signed"
                                           value="{{$profile->date_signed}}">
                                </div>

                                <div class="form-group">
                                    <label for="presumed_death_of_birth">Presumed Death of Birth</label>
                                    <input type="date" name="presumed_death_of_birth" class="form-control"
                                           id="presumed_death_of_birth"
                                           value="{{$profile->presumed_death_of_birth}}">
                                </div>

                                <div class="form-group">
                                    <label for="presumed_time_of_death">Presumed time of Death</label>
                                    <input type="time" name="presumed_time_of_death" class="form-control"
                                           id="presumed_time_of_death"
                                           value="{{$profile->presumed_time_of_death}}">
                                </div>

                                <div class="form-group">
                                    <label for="medical_examiner">Medical Examiner</label>
                                    <input type="text" name="medical_examiner" class="form-control"
                                           id="medical_examiner"
                                           value="{{$profile->medical_examiner}}">
                                </div>

                                <h2>Cause Of Death</h2>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="a_immediate_death">Immediate Death</label>
                                            <input type="text" name="a_immediate_death" class="form-control"
                                                   id="a_immediate_death"
                                                   value="{{$profile->a_immediate_death}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="a_due_to">Due to</label>
                                            <input type="text" name="a_due_to" class="form-control"
                                                   id="a_due_to"
                                                   value="{{$profile->a_due_to}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="a_onset_death">Onset to death</label>
                                            <input type="text" name="a_onset_death" class="form-control"
                                                   id="a_onset_death"
                                                   value="{{$profile->a_onset_death}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="b_sequential_list">Sequential List</label>
                                            <input type="text" name="b_sequential_list" class="form-control"
                                                   id="b_sequential_list"
                                                   value="{{$profile->b_sequential_list}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="b_due_to">Due to</label>
                                            <input type="text" name="b_due_to" class="form-control"
                                                   id="b_due_to"
                                                   value="{{$profile->b_due_to}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="b_onset_death">Onset to death</label>
                                            <input type="text" name="b_onset_death" class="form-control"
                                                   id="b_onset_death"
                                                   value="{{$profile->b_onset_death}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="c_underlying_death">Enter the underlying Death</label>
                                            <input type="text" name="c_underlying_death" class="form-control"
                                                   id="c_underlying_death"
                                                   value="{{$profile->c_underlying_death}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="c_due_to">Due to</label>
                                            <input type="text" name="c_due_to" class="form-control"
                                                   id="c_due_to"
                                                   value="{{$profile->c_due_to}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="c_onset_death">Onset to death</label>
                                            <input type="text" name="c_onset_death" class="form-control"
                                                   id="c_onset_death"
                                                   value="{{$profile->c_onset_death}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="d_last">Last</label>
                                            <input type="text" name="d_last" class="form-control"
                                                   id="d_last"
                                                   value="{{$profile->d_last}}">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="d_onset_death">Onset to death</label>
                                            <input type="text" name="d_onset_death" class="form-control"
                                                   id="d_onset_death"
                                                   value="{{$profile->d_onset_death}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="comment">Comment</label>
                                        <textarea id="comment" name="comment" placeholder="Enter Your Comment">
                                            {{$profile->comment}}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Autospy Performed</label>
                                        <input type="radio" name="performed">Yes
                                        <input type="radio" name="performed">No
                                    </div>
                                    <div class="col-md-4">
                                        <label>Autospy Finding Available</label>
                                        <input type="radio" name="autospy_finding">Yes
                                        <input type="radio" name="autospy_finding">No
                                    </div>
                                    <div class="col-md-4">
                                        <label>Did Tobacco Contribute to Death</label>
                                        <input type="radio" name="tobacco">Yes
                                        <input type="radio" name="tobacco">No
                                        <input type="radio" name="tobacco">Probably
                                        <input type="radio" name="tobacco">Unknown
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>If Female</label><br>
                                    <input type="radio" name="ifFemale" value="Not Pregnant within past year">Not
                                    Pregnant within past year
                                    <input type="radio" name="ifFemale" value="Pregnant at time of death">Pregnant at
                                    time of death
                                    <input type="radio" name="ifFemale"
                                           value="Not Pregnant,but pregnant within 42days of death">Not Pregnant,but
                                    pregnant within 42days of death
                                    <input type="radio" name="ifFemale" value="Not Pregnant,but pregnant within 43days to 1
                                    year before death">Not Pregnant,but pregnant within 43days to 1
                                    year before death
                                    <input type="radio" name="ifFemale"
                                           value="Unknown if pregnant within the past year">Unknown if pregnant within
                                    the past year
                                </div>

                                <div class="form-group">
                                    <label>Manner of Death</label><br>
                                    <input type="radio" name="manner" value="Natural">Natural
                                    <input type="radio" name="manner" value="Homicide">Homicide
                                    <input type="radio" name="manner" value="Accident">Accident
                                    <input type="radio" name="manner" value="Pending Investigation">Pending
                                    Investigation
                                    <input type="radio" name="manner" value="Suicide">Suicide
                                    <input type="radio" name="manner" value="Could not be determined">Could not be
                                    determined
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="date_of_injury">Date of Injury</label>
                                        <input type="date" name="date_of_injury" value="{{$profile->date_of_injury}}"
                                               id="date_of_injury" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="time_of_injury">Time of Injury</label>
                                        <input type="time" name="time_of_injury" value="{{$profile->time_of_injury}}"
                                               id="time_of_injury" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="place_of_injury">Place of Injury</label>
                                        <input type="text" name="place_of_injury" value="{{$profile->place_of_injury}}"
                                               id="place_of_injury" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label>Injury at work</label>
                                        <input type="radio" name="injury_at_work">Yes
                                        <input type="radio" name="injury_at_work">No
                                    </div>
                                </div>

                                <div class="row">
                                    <h1>Location of Injury</h1>
                                    <div class="form-group">
                                        <label for="state">State</label>
                                        <input type="text" id="state" name="state" value="{{$profile->state}}"
                                               class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="city">City or Town</label>
                                        <input type="text" id="city" name="city_or_town"
                                               value="{{$profile->city_or_town}}"
                                               class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="street">Street and Number</label>
                                        <input type="text" id="street" name="street_and_number"
                                               value="{{$profile->street_and_number}}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="apartment">Apartment Number</label>
                                        <input type="text" id="apartment" name="apartment"
                                               value="{{$profile->apartment}}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="zip">Zip Code</label>
                                        <input type="text" id="zip" name="zip"
                                               value="{{$profile->zip}}" class="form-control">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="describe">Describe how injury occurred</label>
                                        <textarea name="describe_injury_occurred">
                                            {{$profile->describe_injury_occurred}}
                                        </textarea>
                                    </div>
                                </div>

                                <div class="row">
                                    <h2>If Transportation Injury</h2>
                                    <div class="form-group">
                                        <label>Specify</label>
                                        <input type="radio" name="specify" value="Driver/Operator">Driver/Operator
                                        <input type="radio" name="specify" value="Passenger">Passenger
                                        <input type="radio" name="specify" value="Pedestrain">Pedestrain
                                        <input type="radio" name="specify" value="Other">Other
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-default w3ls-button">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //input-forms -->
    </div>
@endsection