@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Profiles Table</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">

                @include('backend.layouts.elements.message')
                <div class="text-left">
                    <a href="{{route('profiles.trash')}}" class="btn btn-success text-right" style="margin-bottom: 30px">Go to Trash</a>
                </div>
                <div class="text-right">
                    <a href="{{route('profiles.create')}}" class="btn btn-success text-right" style="margin-bottom: 30px">Add
                        New Profile</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($profiles as $profile)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$profile->first_name}}</td>
                            <td>{{$profile->middle_name}}</td>
                            <td>{{$profile->last_name}}</td>
                            <td>{{$profile->gender}}</td>
                            <td><a href="{{route('profiles.show',$profile->id)}}">Show</a>||<a
                                        href="{{route('profiles.edit',$profile->id)}}">Edit</a>
                                <form action="{{route('profiles.update',$profile->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-success"
                                            onclick="return confirm('Are You Sure want to Delete!')">Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$profiles->links()}}

        </div>
        <!-- //tables -->
    </div>
@endsection