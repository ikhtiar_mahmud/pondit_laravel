@extends('backend.layouts.master')
@section('content')
    <div class="">
        <div class="">
            <p>
                <strong>Name:</strong>{{$profile->first_name." ".$profile->middle_name." ".$profile->last_name}}</p>
            <hr>
            <p class="card-text"><strong>Date Of Birth: </strong>{{$profile->date_of_birth}}</p>
            <hr>
            <p class="card-text"><strong>Gender: </strong>{{$profile->gender}}</p>
            <hr>
            <p class="card-text"><strong>Social Security Number: </strong>{{$profile->social_security_number}}</p>
            <hr>
            <p class="card-text"><strong>Facility Name: </strong>{{$profile->facility_name}}</p>
            <hr>
            <p class="card-text"><strong>Hispanic Origin: </strong>{{$profile->hispanic_origin}}</p>
            <hr>

            <p class="card-text"><strong>Decedent Race: </strong>{{$profile->decedentRace}}</p>
            <hr>

            <p class="card-text"><strong>Date Pronounced Dead: </strong>{{$profile->date_pronounced_dead}}</p>
            <hr>
            <p class="card-text"><strong>Time Pronounced Dead: </strong>{{$profile->time_pronounced_dead}}</p>
            <hr>
            <p class="card-text"><strong>Signature of Person Pronouncing
                    Death: </strong>{{$profile->signature_of_person_pronouncing_death}}</p>
            <hr>

            <p class="card-text"><strong>Licensed Number: </strong>{{$profile->license_number}}</p>
            <hr>
            <p class="card-text"><strong>Date Signed: </strong>{{$profile->date_signed}}</p>
            <hr>
            <p class="card-text"><strong>Presumed Death of Birth: </strong>{{$profile->presumed_death_of_birth}}</p>
            <hr>
            <p class="card-text"><strong>Presumed time of Death: </strong>{{$profile->presumed_time_of_death}}</p>
            <hr>

            <p class="card-text"><strong>Medical Examiner: </strong>{{$profile->medical_examiner}}</p>
            <hr>

            <p class="card-text"><strong>a_immediate_death: </strong>{{$profile->a_immediate_death}}</p>
            <hr>
            <p class="card-text"><strong>a_due_to: </strong>{{$profile->a_due_to}}</p>
            <hr>
            <p class="card-text"><strong>a_onset_death: </strong>{{$profile->a_onset_death}}</p>
            <hr>
            <p class="card-text"><strong>b_sequential_list: </strong>{{$profile->b_sequential_list}}</p>
            <hr>
            <p class="card-text"><strong>b_due_to: </strong>{{$profile->b_due_to}}</p>
            <hr>
            <p class="card-text"><strong>b_onset_death: </strong>{{$profile->b_onset_death}}</p>
            <hr>
            <p class="card-text"><strong>c_underlying_death: </strong>{{$profile->c_underlying_death}}</p>
            <hr>
            <p class="card-text"><strong>c_due_to: </strong>{{$profile->c_due_to}}</p>
            <hr>
            <p class="card-text"><strong>c_onset_death: </strong>{{$profile->c_onset_death}}</p>
            <hr>
            <p class="card-text"><strong>d_last: </strong>{{$profile->d_last}}</p>
            <hr>
            <p class="card-text"><strong>d_onset_death: </strong>{{$profile->d_onset_death}}</p>
            <hr>
            <p class="card-text"><strong>comment: </strong>{{$profile->comment}}</p>
            <hr>
            <p class="card-text"><strong>performed: </strong>{{$profile->performed}}</p>
            <hr>
            <p class="card-text"><strong>autospy_finding: </strong>{{$profile->autospy_finding}}</p>
            <hr>
            <p class="card-text"><strong>tobacco: </strong>{{$profile->tobacco}}</p>
            <hr>
            

            <p class="card-text"><strong>If Female: </strong>{{$profile->ifFemale}}</p>
            <hr>

            <p class="card-text"><strong>Manner of Death : </strong>{{$profile->manner}}</p>
            <hr>


            <p class="card-text"><strong>Date of Injury : </strong>{{$profile->date_of_injury}}</p>
            <hr>


            <p class="card-text"><strong>Time of Injury: </strong>{{$profile->time_of_injury}}</p>
            <hr>

            <p class="card-text"><strong>Place of Injury: </strong>{{$profile->place_of_injury}}</p>
            <hr>

            <p class="card-text"><strong>Injury at work: </strong>{{$profile->injury_at_work}}</p>
            <hr>

            <p class="card-text"><strong>State: </strong>{{$profile->state}}</p>
            <hr>

            <p class="card-text"><strong>City or Town: </strong>{{$profile->city_or_town}}</p>
            <hr>

            <p class="card-text"><strong>Street and Number: </strong>{{$profile->street_and_number}}</p>
            <hr>

            <p class="card-text"><strong>Apartment Number: </strong>{{$profile->apartment}}</p>
            <hr>

            <p class="card-text"><strong>Zip Code: </strong>{{$profile->zip}}</p>
            <hr>

            <p class="card-text"><strong>Describe how injury occurred: </strong>{{$profile->describe_injury_occurred}}
            </p>
            <hr>

            <p class="card-text"><strong>Specify: </strong>{{$profile->specify}}</p>
            <hr>


            <a href="{{url('/profiles/index')}}" class="btn btn-primary">Go Back</a>
        </div>
    </div>
@endsection