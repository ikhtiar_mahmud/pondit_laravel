@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Basic Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">

                @include('backend.layouts.elements.message')
                <div class="text-right">
                    <a href="{{route('profiles.index')}}" class="btn btn-success text-right" style="margin-bottom: 30px">GO to List Page</a>
                </div>

                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($profiles as $profile)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$profile->first_name}}</td>
                            <td>{{$profile->middle_name}}</td>
                            <td>{{$profile->last_name}}</td>
                            <td>{{$profile->gender}}</td>
                            <td><a href="{{route('profiles.restore',$profile->id)}}">Restore</a>
                                <form action="{{route('profiles.delete',$profile->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-success"
                                            onclick="return confirm('Are You Sure want to Delete!')">Delete
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$profiles->links()}}

        </div>
        <!-- //tables -->
    </div>
@endsection