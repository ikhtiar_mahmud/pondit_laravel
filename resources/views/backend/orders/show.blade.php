@extends('backend.layouts.master')
@section('content')
    <div class="card bg-white">
        <div class="card-header bg-info">
            <a href="{{ route('orders.index') }}" class="btn btn-success">List Page</a>
        </div>
        <br>
        <div class="card-body">
            
            <p><strong>User ID : </strong> {{ $order->user_id }}</p>
            <hr>
            <p><strong>Product ID : </strong> {{ $order->product_id }}</p>
            <hr>
            <p><strong>Size ID : </strong> {{ $order->size_id }}</p>
            <hr>
            <p><strong>Quantity : </strong> {{ $order->quantity }}</p>
            <hr>
            <p><strong>Unit Price : </strong> {{ $order->unit_price }} Tk</p>
            <hr>
            <p><strong>Total Price : </strong> {{ $order->total_price }} Tk</p>
            <hr>
            <p><strong>Is Delivered : </strong> {{ $order->is_delivered }} </p>
            <hr>
            <p><strong>Updated By : </strong> {{ $order->updated_by }} </p>
            <hr>
            <p><strong>Deleted By : </strong> {{ $order->deleted_by }}</p>
            <hr>

        </div>
        <div class="card-footer"></div>
    </div>
@endsection