@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Trashed Table</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @include('backend.layouts.elements.message')

                <div class="text-left" style="margin-bottom: 20px">
                    <a href="{{route('products.index')}}" class="btn btn-danger">product List</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    @foreach($products as $product)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$product->title}}</td>
                            <td><a href="{{route('products.restore',$product->id)}}" class="btn btn-warning">Restore</a>
                                <form action="{{route('products.delete',$product->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$products->links()}}
        </div>
        <!-- //tables -->
    </div>
@endsection