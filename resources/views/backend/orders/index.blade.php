@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Orders Table</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @include('backend.layouts.elements.message')
                
                <table id="table" class="table table-bordered table-striped text-center">
                    <tr>
                        <th>SL</th>
                        <th>Product ID</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                        <th>Is Delivered</th>
                        <th>Action</th>
                    </tr>
                   
                    @foreach($orders as $order)
                        <tr>
                            <td>{{++$serial}}</td>
                          
                            <td>{{$order->product_id}}</td>
                            <td>{{$order->quantity}}</td>
                            <td>{{$order->total_price}}</td>
                            <td>{{$order->is_delivered}}</td>
                           
                            <td><a href="{{route('orders.edit',$order->id)}}" class="btn btn-warning">Edit</a>
                                <a href="{{route('orders.show',$order->id)}}" class="btn btn-success">Show</a>
                                <form action="{{route('orders.destroy',$order->id)}}" method="post" style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$orders->links()}}
        </div>
        <!-- //tables -->
    </div>
@endsection