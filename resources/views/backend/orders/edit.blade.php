@extends('backend.layouts.master')
@section('content')

    <div class="agile-grids">
        <!-- input-forms -->
        <div class="grids">
            <div class="progressbar-heading grids-heading">
                <h2>Order Edit</h2>
            </div>
            <div class="panel panel-widget forms-panel">
                <div class="forms">
                    <div class="form-grids widget-shadow" data-example-id="basic-forms">
                        <div class="form-title">
                            @include('backend.layouts.elements.error')
                        </div>
                        <div class="form-body">
                            {{ Form::model($order,['route'=>['orders.update',$order->id],'method'=>'put','enctype'=>'multipart/form-data'])}}

                            @include('backend.orders.form')

                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //input-forms -->
    </div>

@endsection