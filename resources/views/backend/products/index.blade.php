@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Basic Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @include('backend.layouts.elements.message')

                <div class="text-left" style="margin-bottom: 20px">
                    <a href="{{route('products.trash')}}" class="btn btn-danger">Go to trash</a>
                </div>
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('products.create')}}" class="btn btn-success">Add New Products</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <tr>
                        <th>Sl</th>
                        <th>Title</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    @foreach($products as $product)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$product->title}}</td>
                            <td>
                                @if(file_exists(public_path().'/uploads/products/'.$product->image) && (!is_null($product->image)))
                                    <img src="{{asset('/uploads/products/'.$product->image)}}" width="100px"
                                         height="100px">
                                @else
                                    <img src="{{ asset('/uploads/default.png') }}" height="100">
                                @endif

                            </td>
                            <td><a href="{{route('products.edit',$product->id)}}" class="btn btn-warning">Edit</a> <a
                                        href="{{route('products.show',$product->id)}}"
                                        class="btn btn-success">Show</a>
                                <form action="{{route('products.destroy',$product->id)}}" method="post" style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
            {{$products->links()}}
        </div>
        <!-- //tables -->
    </div>
@endsection