{!! Form::label('category_id','Category Name') !!}
{!! Form::select('category_id',$categories,null,['class'=>'form-control']) !!}<br>

{!! Form::label('brand_id_id','Brand Name') !!}
{!! Form::select('brand_id',$brand,null,['class'=>'form-control']) !!}<br>

{!! Form::label('title','Product Title') !!}
{!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Enter your product title']) !!}<br>

{!! Form::label('image','Product image') !!}
{!! Form::file('image',null,['class'=>'form-control','placeholder'=>'Enter your product image']) !!}<br>

{!! Form::label('short_description','Product Short Description') !!}
{!! Form::textarea('short_description',null,['class'=>'form-control','placeholder'=>'Enter your product short description','id'=>'whats-new']) !!}
<br>

{!! Form::label('long description','Product long description') !!}
{!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter your product long description','id'=>'whats-new']) !!}
<br>

{!! Form::label('price','Product price') !!}
{!! Form::text('price',null,['class'=>'form-control','placeholder'=>'Enter your product price']) !!}<br>

{!! Form::label('discount','Product Discount Percentage') !!}
{!! Form::text('discount',null,['class'=>'form-control','placeholder'=>'Enter your product discount percentage']) !!}<br>


{!! Form::label('tag_id','Tags: ') !!}
@foreach($tags as $key=>$tag)
    {!! Form::checkbox('tag_id[]',$key, in_array($key, $selectedTagIds)) !!}
    {!! Form::label('tag_id',$tag) !!}
@endforeach
<br><br>

{!! Form::label('size_id','Sizes: ') !!}
@foreach($sizes as $key=>$size)
    {!! Form::checkbox('size_id[]',$key, in_array($key, $selectedSizeIds)) !!}
    {!! Form::label('size_id',$size) !!}
@endforeach
<br><br>

<div class="text-center">
    {!! Form::button('Submit',['type'=>'submit','class'=>'btn btn-danger','style'=>'cursor:pointer']) !!}
</div>

