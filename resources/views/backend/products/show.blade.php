@extends('backend.layouts.master')
@section('content')
    <div class="card bg-white">
        <div class="card-header bg-info">
            <a href="{{ route('products.index') }}" class="btn btn-success">List Page</a>
        </div>
        <div class="card-body">
            <p><strong>Title : </strong> {{ $product->title }}</p>
            <hr>
            <p><strong>Category Name : </strong> {{ $product->category->title }}</p>
            <hr>
            <p><strong>Brand Name : </strong> {{ $product->brand->name }}</p>
            <hr>
            <p><strong>Tags : </strong>
                @foreach($product->tags as $tag)
                    <span>{{$tag->title }}</span>
                @endforeach
            </p>
            <hr>
            <p><strong>Sizes : </strong>
                @foreach($product->sizes as $size)
                    <span>{{$size->title}}</span>
                @endforeach
            </p>
            <hr>
            <p><strong>Short Description : </strong> {!! $product->short_description !!}</p>
            <hr>
            <p><strong>Long Description : </strong> {!! $product->description !!}</p>
            <hr>
            <p><strong>Price : </strong> {{ $product->price }}</p>
            <hr>

        </div>
        <div class="card-footer"></div>
    </div>
@endsection