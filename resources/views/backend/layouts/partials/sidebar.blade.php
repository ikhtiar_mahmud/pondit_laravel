<nav class="main-menu">
    <ul>
        <li>
            <a href="{{route('home')}}">
                <i class="fa fa-home nav_icon"></i>
                <span class="nav-text">
					Dashboard
					</span>
            </a>
        </li>

        <li>
            <a href="{{route('products.index')}}">
                <i class="fa fa-check-square-o nav_icon"></i>
                <span class="nav-text">
						Products
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('categories.index')}}">
                <i class="fa fa-list-ul nav_icon"></i>
                <span class="nav-text">
						Categories
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('tags.index')}}">
                <i class="fa fa-list-ul nav_icon"></i>
                <span class="nav-text">
						Tags
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('brands.index')}}">
                <i class="fa fa-map-marker nav_icon"></i>
                <span class="nav-text">
						Brands
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('colors.index')}}">
                <i class="fa fa-map-marker nav_icon"></i>
                <span class="nav-text">
						Colors
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('sizes.index')}}">
                <i class="fa fa-map-marker nav_icon"></i>
                <span class="nav-text">
						Size
					</span>
            </a>
        </li>
        <li>
            <a href="{{route('orders.index')}}">
                <i class="fa fa-map-marker nav_icon"></i>
                <span class="nav-text">
						Orders
					</span>
            </a>
        </li>

    </ul>
    <ul class="logout">
        <li>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="icon-off nav-icon"></i>
                <span class="nav-text">
                    {{ __('Logout') }}
                </span>
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>