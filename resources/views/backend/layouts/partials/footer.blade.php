<div class="footer">
    <p>© 2016 Colored . All Rights Reserved . Design by <a href="http://w3layouts.com/">W3layouts</a></p>
</div>
</section>
<script src="{{asset('ui/backend/js/bootstrap.js')}}"></script>
<script src="{{asset('ui/backend/js/proton.js')}}"></script>
<!-- TinyMCE -->
<script src="{{asset('ui/tinymce/tinymce.jquery.js')}}"></script>
<script src="{{asset('ui/tinymce/tinymce.js')}}"></script>
<script type="text/javascript">

    //TinyMCE
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        height: 200,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{asset('ui/tinymce')}}';

</script>

</body>
</html>