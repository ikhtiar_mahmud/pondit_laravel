@extends('backend.layouts.master')
@section('content')
    <div class="card bg-white">
        <div class="card-header bg-info">
            <a href="{{ route('brands.index') }}" class="btn btn-success">List Page</a>
        </div>
        <div class="card-body">
            <p><strong>Short name : </strong> {{ $brands->short_name }}</p>
            <hr>
            <p><strong>Name : </strong> {{ $brands->name }}</p>
            <hr>
            <p><strong>Description : </strong> {{ $brands->description }}</p>
            <hr>
            <p><strong>Logo : </strong> {{ $brands->logo}}</p>
            <hr>
        </div>
        <div class="card-footer"></div>
    </div>
@endsection