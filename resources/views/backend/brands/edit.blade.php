@extends('backend.layouts.master')
@section('content')

    <div class="agile-grids">
        <!-- input-forms -->
        <div class="grids">
            <div class="progressbar-heading grids-heading">
                <h2>Edit Brand</h2>
            </div>
            <div class="panel panel-widget forms-panel">
                <div class="forms">
                    <div class="form-grids widget-shadow" data-example-id="basic-forms">
                        <div class="form-title">
                            @include('backend.layouts.elements.error')
                        </div>
                        <div class="form-body">
                            <form action="{{route('brands.update',$brands->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label for="short_name">Short Name</label>
                                    <input type="text" name="short_name" class="form-control" id="short_name" value="{{$brands->short_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{$brands->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea class="form-control" id="description" rows="2"  name="description" >{{$brands->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="logo">Logo</label>
                                    <input type="file" class="form-control" id="logo" name="logo">
                                </div>
                                <div class="form-group">
                                    <label for="created_by">created_by</label>
                                    <input type="text" id="created_by" value="{{$brands->created_by}}" name="created_by" class="form-control" >
                                </div>
                                <div class="form-group">
                                    <label for="updated_by">updated_by</label>
                                    <input type="text" id="updated_by" value="{{$brands->updated_by}}" name="updated_by" class="form-control" >
                                </div>

                                <button type="submit" class="btn btn-danger">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //input-forms -->
    </div>

@endsection