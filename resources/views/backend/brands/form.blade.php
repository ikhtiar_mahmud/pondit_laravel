<div class="form-group">
    {!! Form::label('short_name','Short Name') !!}
    {!! Form::text('short_name',null,['class'=>'form-control','placeholder'=>'Enter your Brand Short Name']) !!}
</div>
<div class="form-group">
    {!! Form::label('name','Name') !!}
    {!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter your Brand Name']) !!}
</div>
<div class="form-group">
    {!! Form::label('description','Brand Description') !!}
    {!! Form::textarea('description',null,['class'=>'form-control','placeholder'=>'Enter your Brand Description','rows'=>2]) !!}
</div>
<div class="form-group">
    {!! Form::label('brand_logo','Brand Logo') !!}
    {!! Form::file('logo',null,['class'=>'form-control','placeholder'=>'Enter your Brand Logo']) !!}
</div>

{!! Form::button('submit',['type'=>'submit','class'=>'btn btn-default w3ls-button']) !!}
