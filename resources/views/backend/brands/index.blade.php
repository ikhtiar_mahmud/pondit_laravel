@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Basic Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @include('backend.layouts.elements.message')

                {{--<div class="text-left" style="margin-bottom: 20px">
                    <a href="{{route('products.trash')}}" class="btn btn-danger">Go to trash</a>
                </div>--}}
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('brands.create')}}" class="btn btn-success">Add New Brands</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <tr>
                        <th>Sl</th>
                        <th>Short_name</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Logo</th>
                        <th>Action</th>
                    </tr>
                    @foreach($brands as $brand)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$brand->short_name}}</td>
                            <td>{{$brand->name}}</td>
                            <td>{{$brand->description}}</td>
                            <td>
                                @if(file_exists(public_path().'/uploads/brands/'.$brand->logo) && (!is_null($brand->logo)))
                                    <img src="{{asset('/uploads/brands/'.$brand->logo)}}" width="100px"
                                         height="100px">
                                @endif

                            </td>

                            <td>
                                <a href="{{route('brands.show',$brand->id)}}" class="btn btn-success">Show</a>
                                <a href="{{route('brands.edit',$brand->id)}}" class="btn btn-warning">Edit</a>

                                <form action="{{route('brands.destroy',$brand->id)}}" method="post" style="display: inline;">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
          {{--  {{$brands->links()}}--}}
        </div>
        <!-- //tables -->
    </div>
@endsection