@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Trash List</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @if(session('message'))
                    <div class="alert alert-success">
                        {{{session('message')}}}
                    </div>
                @endif
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('tags.index')}}" class="btn btn-success">Go to List</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tags as $tag)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$tag->title}}</td>
                            <td><a href="{{ route('tags.restore',$tag->id) }}">Restore</a>
                                <form action="{{route('tags.delete',$tag->id)}}" method="post">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$tags->links()}}

        </div>
    </div>
@endsection