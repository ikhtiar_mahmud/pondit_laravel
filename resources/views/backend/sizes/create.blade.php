@extends('backend.layouts.master')
@section('content')

    <div class="agile-grids">
        <!-- input-forms -->
        <div class="grids">
            <div class="progressbar-heading grids-heading">
                <h2>Sizes Forms</h2>
            </div>
            <div class="panel panel-widget forms-panel">
                <div class="forms">
                    <div class="form-grids widget-shadow" data-example-id="basic-forms">
                        <div class="form-title">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <div class="form-body">
                            {!! Form::open(['route'=>'sizes.store','method'=>'post']) !!}
                                @include('backend.sizes.form')
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //input-forms -->
    </div>

@endsection