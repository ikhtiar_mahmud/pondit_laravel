@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Trash List</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @if(session('message'))
                    <div class="alert alert-success">
                        {{{session('message')}}}
                    </div>
                @endif
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('sizes.index')}}" class="btn btn-success">Go to List</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sizes as $size)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$size->title}}</td>
                            <td><a href="{{ route('sizes.restore',$size->id) }}" class="btn btn-primary">Restore</a>
                                <form action="{{route('sizes.delete',$size->id)}}" method="post" style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$sizes->links()}}

        </div>
    </div>
@endsection