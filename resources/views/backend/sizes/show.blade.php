@extends('backend.layouts.master')
@section('content')


    <div class="card bg-white">
        <div class="card-header bg-info">
            <a href="{{ url('/sizes') }}" class="btn btn-success">List</a>
        </div>
        <div class="card-body">
            <p><strong>Title : </strong> {{ $size->title }}</p>

        </div>
        <div class="card-footer"></div>
    </div>


@endsection