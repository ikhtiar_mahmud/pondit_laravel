@extends('backend.layouts.master')
@section('content')
    <div class="agile-grids">
        <!-- tables -->

        <div class="table-heading">
            <h2>Size Tables</h2>
        </div>
        <div class="agile-tables">
            <div class="w3l-table-info">
                @if(session('message'))
                    <div class="alert alert-success">
                        {{{session('message')}}}
                    </div>
                @endif
                <div class="text-left" style="margin-bottom: 20px">
                    <a href="{{ route('sizes.trash')}}" class="btn btn-success">Go to Trash</a>
                </div>
                <div class="text-right" style="margin-bottom: 20px">
                    <a href="{{route('sizes.create')}}" class="btn btn-success">Add New Sizes</a>
                </div>
                <table id="table" class="table table-bordered table-striped text-center">
                    <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($sizes as $size)
                        <tr>
                            <td>{{++$serial}}</td>
                            <td>{{$size->title}}</td>
                            <td><a href="{{ route('sizes.show',$size->id) }}" class="btn btn-primary">Show</a>
                                <a
                                        href="{{ route('sizes.edit',$size->id) }}" class="btn btn-warning">Edit</a>
                                <form action="{{route('sizes.destroy',$size->id)}}" method="post"
                                      style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$sizes->links()}}

        </div>
    </div>
@endsection
