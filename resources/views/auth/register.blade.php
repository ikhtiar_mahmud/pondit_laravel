@include('backend.layouts.partials.header')


<body class="signup-body">
<div class="agile-signup">

    <div class="content2">
        <div class="grids-heading gallery-heading signup-heading">
            <h2>Sign Up</h2>
        </div>
        <form method="post" action="{{ route('register') }}">
            @csrf
            @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
            <input id="name" type="text" placeholder="Enter Your Name"
                   class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                   value="{{ old('name') }}" required autofocus>

            <input id="email" type="email" placeholder="Enter Your Email Address"
                   class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
                   required>

            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
            <input id="password" placeholder="Enter Password" type="password"
                   class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
            <input id="password-confirm" placeholder="Confirm Password" type="password" class=""
                   name="password_confirmation" required>

            {{--<input id="image" type="file"--}}
                   {{--class="{{ $errors->has('image') ? ' is-invalid' : '' }}" style="margin-left: 20px;--}}
{{--margin-top: 12px;" name="image" required>--}}


            <input id="facebook_link" type="text"
                   class="" name="facebook_link" placeholder="Facebook Link" required>

            <input id="twitter_link" type="text"
                   class="" name="twitter_link" placeholder="Twitter Link" required>

            <button type="submit" class="register">
                {{ __('Register') }}
            </button>
        </form>
        <a href="{{route('login')}}">Login</a>
    </div>


</div>




