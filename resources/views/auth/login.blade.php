@include('backend.layouts.partials.header')

<body class="signup-body">
<div class="agile-signup">

    <div class="content2">
        <div class="grids-heading gallery-heading signup-heading">
            <h2>Login</h2>
        </div>
        <form action="{{ route('login') }}" method="post">
            @csrf
            <input type="text" placeholder="Enter your Email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}"
                   name="email"
                   value="{{ old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
            @endif
            <input type="password" placeholder="Enter your Password"
                   class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
            @endif
            <input type="submit" class="register" value="Login">
        </form>
        <div class="signin-text">
            <div class="text-center">
                <p><input class="form-check-input" type="checkbox" name="remember"
                          id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label></p>
            </div>
        </div>
        <br>
        <div class="signin-text">
            <div class="text-left">
                <p> @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif</p>
            </div>
            <div class="text-right">
                <p><a href="{{route('register')}}"> Create New Account</a></p>
            </div>
            <div class="clearfix"></div>
        </div>

        <h5>- OR -</h5>
        <div class="footer-icons">
            <ul>
                <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="twitter facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="twitter chrome"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="twitter dribbble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
            </ul>
        </div>
        <a href="{{route('front.index')}}">Back To Home</a>
    </div>

</div>

