<table>
    <tr>
        <th>SL</th>
        <th>TITLE</th>
    </tr>
    @php $serial=0 @endphp
    @foreach($categories as $category)
        <tr>
            <td>{{++$serial}}</td>
            <td>{{$category->title}}</td>
        </tr>
    @endforeach
</table>
