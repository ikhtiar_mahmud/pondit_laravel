@extends('frontend.layouts.master')
@section('content')
    @include('frontend.layouts.elements.breadcrumb')
    <section class="ab-info-main py-md-5 py-4">
        <div class="container py-md-3">
            <!-- top Products -->
            <div class="row">
                <!-- product left -->
            @include('frontend.layouts.elements.sidebar')
            <!-- //product left -->
                <!-- product right -->
                <div class="left-ads-display col-lg-8">
                    <div class="row">
                        <div class="desc1-left col-md-6">
                            <img src="{{asset('uploads/products/'.$product->image)}}" class="img-fluid" alt="">
                        </div>
                        <div class="desc1-right col-md-6 pl-lg-4">
                            <h3>{{$product->title}}</h3>
                            @php
                                $discount=($product->price*$product->discount)/100;
                            @endphp
                            @if(!($product->discount)==null)
                                <h5>BDT. {{$product->price-$discount}} <span>{{$product->price}}</span></h5>
                            @else
                                <h5>BDT. {{$product->price}}</h5>
                            @endif
                            <div class=" mt-3">
                                <form action="#" method="post" class="">
                                    <div class="form-group">
                                        <input type="text" name="phone" placeholder="Enter your phone"
                                               required="" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="quantity" class="form-control"
                                               placeholder="Enter your qty" required="">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="size" class="form-control"
                                               placeholder="Enter your size"
                                               required="">
                                    </div>

                                    <button class="btn btn-primary">ORDER</button>

                                </form>
                                <span><a href="#">login to save in wishlist </a></span>
                                <p>{!! $product->short_description !!}</p>
                            </div>
                            <div class="share-desc">
                                <div class="share">
                                    <h4>Share Product :</h4>
                                    <ul class="w3layouts_social_list list-unstyled">
                                        <li>
                                            <a href="#" class="w3pvt_facebook">
                                                <span class="fa fa-facebook-f"></span>
                                            </a>
                                        </li>
                                        <li class="mx-2">
                                            <a href="#" class="w3pvt_twitter">
                                                <span class="fa fa-twitter"></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" class="w3pvt_dribble">
                                                <span class="fa fa-dribbble"></span>
                                            </a>
                                        </li>
                                        <li class="ml-2">
                                            <a href="#" class="w3pvt_google">
                                                <span class="fa fa-google-plus"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row sub-para-w3layouts mt-5">

                        <h3 class="shop-sing">{{$product->title}}</h3>
                        <p>{!! $product->description !!}</p>
                    </div>
                    <div class="row mt-5">
                        <form action="{{route('front.comment',$product->id)}}" method="post" style="width: 100%">
                            @csrf
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea rows="5" class="form-control" id="comment" name="comment"></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </form>
                    </div>


                    <div class="row mt-5">
                        @if(count($comments) > 0)
                            <h3 class="">Comments ({{count($comments)}})</h3>
                        @else
                            <h3>Comment</h3>
                        @endif

                        <div class="container">
                            @foreach($comments as $comment)
                                <div class="row">
                                    <h5>{{auth()->user()->name}}</h5>
                                    <div class="container">
                                        <p>{{$comment->body}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>


                    <div class="row sub-para-w3layouts mt-5">
                        <span>Tags: &nbsp;&nbsp;</span>
                        @foreach($product->tags as $tag)
                            <a href="{{route('front.tag_base_shop',$tag->id)}}">{{$tag->title}}</a>&nbsp;&nbsp;
                        @endforeach

                    </div>

                    <h3 class="shop-sing">Featured Products</h3>
                    <div class="row m-0">

                        @foreach($product->category->products as $category)
                            <div class="col-md-4 product-men">
                                <div class="product-shoe-info shoe text-center">
                                    <div class="men-thumb-item">
                                        <img src="{{asset('uploads/products/'.$category->image)}}" class="img-fluid"
                                             alt="">
                                    </div>
                                    <div class="item-info-product">
                                        <h4>
                                            <a href="{{route('front.shop_single',$category->id)}}">{{$category->title}}</a>
                                        </h4>

                                        <div class="product_price">
                                            <div class="grid-price">
                                                <span class="money">$675.00</span>
                                            </div>
                                        </div>
                                        <ul class="stars">
                                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                            <li><a href="#"><span class="fa fa-star-half-o"
                                                                  aria-hidden="true"></span></a>
                                            </li>
                                            <li><a href="#"><span class="fa fa-star-half-o"
                                                                  aria-hidden="true"></span></a>
                                            </li>
                                            <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
