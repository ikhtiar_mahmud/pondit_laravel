<div class="side-bar col-lg-4">

    <div class="search-bar w3layouts-newsletter">
        <h3 class="sear-head">Search Here..</h3>
        <form action="#" method="post" class="d-flex">
            <input type="search" placeholder="Product name..." name="search" class="form-control"
                   required="">
            <button class="btn1"><span class="fa fa-search" aria-hidden="true"></span></button>
        </form>
    </div>

    <div class="left-side my-4">
        <h3 class="sear-head">Brands</h3>
        <ul class="w3layouts-box-list">
            @foreach($brands as $brand)
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span">{{$brand->name}}</span>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- // preference -->
    <!-- discounts -->
    <div class="left-side">
        <h3 class="sear-head">Discount</h3>
        <ul class="w3layouts-box-list">
            @foreach($discounts as $discount)
                <li>
                    <input type="checkbox" class="checked">
                    <span class="span"> {{$discount->discount}} %</span>
                </li>
            @endforeach
        </ul>
    </div>
    <!-- //discounts -->

    <!-- deals -->
    <div class="deal-leftmk left-side">
        <h3 class="sear-head">Special Deals</h3>
        <div class="special-sec1 row mb-3">
            <div class="img-deals col-md-4">
                <img src="{{asset('ui/frontend/images/s4.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="img-deal1 col-md-4">
                <h3>Shuberry Heels</h3>
                <a href="shop-single.html">$180.00</a>
            </div>

        </div>
        <div class="special-sec1 row">
            <div class="img-deals col-md-4">
                <img src="{{asset('ui/frontend/images/s2.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="img-deal1 col-md-8">
                <h3>Chikku Loafers</h3>
                <a href="shop-single.html">$99.00</a>
            </div>

        </div>
        <div class="special-sec1 row my-3">
            <div class="img-deals col-md-4">
                <img src="{{asset('ui/frontend/images/s1.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="img-deal1 col-md-8">
                <h3>Bella Toes</h3>
                <a href="shop-single.html">$165.00</a>
            </div>

        </div>
        <div class="special-sec1 row">
            <div class="img-deals col-md-4">
                <img src="{{asset('ui/frontend/images/s5.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="img-deal1 col-md-8">
                <h3>Red Bellies</h3>
                <a href="shop-single.html">$225.00</a>
            </div>

        </div>
        <div class="special-sec1 row mt-3">
            <div class="img-deals col-md-4">
                <img src="{{asset('ui/frontend/images/s3.jpg')}}" class="img-fluid" alt="">
            </div>
            <div class="img-deal1 col-md-8">
                <h3>(SRV) Sneakers</h3>
                <a href="shop-single.html">$169.00</a>
            </div>

        </div>
    </div>
    <!-- //deals -->

</div>