<nav class="py-4" style="background-color: #138AFE">
    <div id="logo">
        <h1><a href="{{route('front.index')}}"><span class="fa fa-bold" aria-hidden="true"></span>ootie</a></h1>
    </div>

    <label for="drop" class="toggle">Menu</label>
    <input type="checkbox" id="drop"/>
    <ul class="menu mt-2">
        <li class="active"><a href="{{route('front.index')}}">Home</a></li>
        <li><a href="{{route('front.about')}}">About</a></li>
        <li><a href="{{route('front.blog')}}">Blog</a></li>
        <li>
            <!-- First Tier Drop Down -->
            <label for="drop-2" class="toggle">Drop Down <span class="fa fa-angle-down"
                                                               aria-hidden="true"></span> </label>
            <a href="#">Drop Down <span class="fa fa-angle-down" aria-hidden="true"></span></a>
            <input type="checkbox" id="drop-2"/>
            <ul>
                <li><a href="{{route('front.blog')}}">Blog</a></li>
                <li><a href="">Shop Now</a></li>
                <li><a href="{{route('front.single')}}">Single Page</a></li>
            </ul>
        </li>
        <li><a href="{{route('front.contact')}}">Contact</a></li>
    </ul>
</nav>