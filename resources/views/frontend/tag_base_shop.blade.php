@extends('frontend.layouts.master')
@section('content')

    @include('frontend.layouts.elements.breadcrumb')

    <section class="ab-info-main py-md-5 py-4">
        <div class="container py-md-3">
            <!-- top Products -->
            <div class="row">
                <!-- product left -->
                @include('frontend.layouts.elements.sidebar')
                <!-- //product left -->
                <!-- product right -->
                <div class="left-ads-display col-lg-8">
                    <div class="row">

                        @foreach($products->products as $product)
                        <div class="col-md-4 product-men">
                            <div class="product-shoe-info shoe text-center">
                                <div class="men-thumb-item">
                                    <img src="{{asset('uploads/products/'.$product->image)}}" class="img-fluid" alt="">
                                </div>
                                <div class="item-info-product">
                                    <h4>
                                        <a href="{{route('front.shop_single',$product->id)}}">{{$product->title}}</a>
                                    </h4>

                                    <div class="product_price">
                                        <div class="grid-price">
                                            <span class="money">{{$product->price}} Tk</span>
                                        </div>
                                    </div>
                                    <ul class="stars">
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star" aria-hidden="true"></span></a></li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a>
                                        </li>
                                        <li><a href="#"><span class="fa fa-star-half-o" aria-hidden="true"></span></a>
                                        </li>
                                        <li><a href="#"><span class="fa fa-star-o" aria-hidden="true"></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>


                    <div class="grid-img-right mt-4 text-right">
                        <span class="money">Flat 50% Off</span>
                        <a href="shop-single.html" class="btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection